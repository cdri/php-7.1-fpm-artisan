# php-7.1-fpm-artisan
[Coderi : Inteligência Empresarial](http://www.coderi.com.br) | [DockerHub](https://cloud.docker.com/swarm/kamihouse/repository/docker/kamihouse/php-7.1-fpm-artisan) custom image.

Workdir: `/app`

### Sample service

```
services:
  artisan:
    image: "kamihouse/php-7.1-fpm-artisan:latest"
    volumes:
      - ".:/app"
    command: ["php", "artisan", "migrate"]
```

### Utilities

* php-7.1-fpm
* pdo
* pdo_pgsql (libpq-dev)
* pdo_mysql
* pdo_sqlite
